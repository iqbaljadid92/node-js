// // server.js
// import express from 'express';
// import ReflectionJSObject from '../src/usingJSObject/controllers/Reflection';
// import ReflectionusingDB from '../src/usingDB/controllers/Reflection';
// import 'babel-polyfill';

// const app = express()

// app.use(express.json())

// app.get('/', (req, res) => {
//   return res.status(200).send({'message': 'YAY! Congratulations! Your first endpoint is working'});
// })

// app.post('/api/v1/reflections', ReflectionusingDB.create);
// app.get('/api/v1/reflections', ReflectionusingDB.getAll);
// app.get('/api/v1/reflections/:id', ReflectionusingDB.getOne);
// app.put('/api/v1/reflections/:id', ReflectionusingDB.update);
// app.delete('/api/v1/reflections/:id', ReflectionusingDB.delete);

// app.listen(3000)
// console.log('app running on port ', 3000);


// server.js
import express from 'express';
import dotenv from 'dotenv';
import 'babel-polyfill';
import ReflectionWithJsObject from '../src/usingJSObject/controllers/Reflection';
import ReflectionWithDB from '../src/usingDB/controllers/Reflection';
// import UserWithDb from '../src/usingDB/controllers/Users';
import Auth from '../src/usingDB/middleware/Auth';
import Register from './usingDB/controllers/Register';

dotenv.config();
const Reflection = process.env.TYPE === 'db' ? ReflectionWithDB : ReflectionWithJsObject;
const app = express()

app.use(express.json())

app.get('/', (req, res) => {
  return res.status(200).send({'message': 'YAY! Congratulations! Your first endpoint is working'});
});

app.post('/api/v1/reflections', Auth.verifyToken, Reflection.create);
app.get('/api/v1/reflections', Auth.verifyToken, Reflection.getAll);
app.get('/api/v1/reflections/:id', Auth.verifyToken, Reflection.getOne);
app.put('/api/v1/reflections/:id', Auth.verifyToken, Reflection.update);
app.delete('/api/v1/reflections/:id', Auth.verifyToken, Reflection.delete);
app.post('/api/v1/register', Register.registerNew);
// app.post('/api/v1/users', UserWithDb.create);
// app.post('/api/v1/users/login',UserWithDb.login);
// app.delete('/api/v1/users/me', Auth.verifyToken, UserWithDb.delete);

app.listen(3003)
console.log('app running on port ', 3003);
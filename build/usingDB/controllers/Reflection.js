"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _uuid = require("uuid");

var _index = _interopRequireDefault(require("../db/index"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// const Reflection = {
//   /**
//    * Create A Reflection
//    * @param {object} req 
//    * @param {object} res
//    * @returns {object} reflection object 
//    */
//   async create(req, res) {
//     const text = `INSERT INTO
//       reflections(id, success, low_point, take_away, created_date, modified_date)
//       VALUES($1, $2, $3, $4, $5, $6)
//       returning *`;
//     const values = [
//       uuidv4(),
//       req.body.success,
//       req.body.low_point,
//       req.body.take_away,
//       moment(new Date()),
//       moment(new Date())
//     ];
//     try {
//       const { rows } = await db.query(text, values);
//       return res.status(201).send(rows[0]);
//     } catch(error) {
//       return res.status(400).send(error);
//     }
//   },
//   /**
//    * Get All Reflection
//    * @param {object} req 
//    * @param {object} res 
//    * @returns {object} reflections array
//    */
//   async getAll(req, res) {
//     const findAllQuery = 'SELECT * FROM reflections';
//     try {
//       const { rows, rowCount } = await db.query(findAllQuery);
//       return res.status(200).send({ rows, rowCount });
//     } catch(error) {
//       return res.status(400).send(error);
//     }
//   },
//   /**
//    * Get A Reflection
//    * @param {object} req 
//    * @param {object} res
//    * @returns {object} reflection object
//    */
//   async getOne(req, res) {
//     const text = 'SELECT * FROM reflections WHERE id = $1';
//     try {
//       const { rows } = await db.query(text, [req.params.id]);
//       if (!rows[0]) {
//         return res.status(404).send({'message': 'reflection not found'});
//       }
//       return res.status(200).send(rows[0]);
//     } catch(error) {
//       return res.status(400).send(error)
//     }
//   },
//   /**
//    * Update A Reflection
//    * @param {object} req 
//    * @param {object} res 
//    * @returns {object} updated reflection
//    */
//   async update(req, res) {
//     const findOneQuery = 'SELECT * FROM reflections WHERE id=$1';
//     const updateOneQuery =`UPDATE reflections
//       SET success=$1,low_point=$2,take_away=$3,modified_date=$4
//       WHERE id=$5 returning *`;
//     try {
//       const { rows } = await db.query(findOneQuery, [req.params.id]);
//       if(!rows[0]) {
//         return res.status(404).send({'message': 'reflection not found'});
//       }
//       const values = [
//         req.body.success || rows[0].success,
//         req.body.low_point || rows[0].low_point,
//         req.body.take_away || rows[0].take_away,
//         moment(new Date()),
//         req.params.id
//       ];
//       const response = await db.query(updateOneQuery, values);
//       return res.status(200).send(response.rows[0]);
//     } catch(err) {
//       return res.status(400).send(err);
//     }
//   },
//   /**
//    * Delete A Reflection
//    * @param {object} req 
//    * @param {object} res 
//    * @returns {void} return statuc code 204 
//    */
//   async delete(req, res) {
//     const deleteQuery = 'DELETE FROM reflections WHERE id=$1 returning *';
//     try {
//       const { rows } = await db.query(deleteQuery, [req.params.id]);
//       if(!rows[0]) {
//         return res.status(404).send({'message': 'reflection not found'});
//       }
//       return res.status(204).send({ 'message': 'deleted' });
//     } catch(error) {
//       return res.status(400).send(error);
//     }
//   }
// }
// export default Reflection;
var Reflection = {
  /**
   * Create A Reflection
   * @param {object} req 
   * @param {object} res
   * @returns {object} reflection object 
   */
  create: function create(req, res) {
    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      var createQuery, values, _yield$db$query, rows;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              createQuery = "INSERT INTO\n      reflections(id, success, low_point, take_away, owner_id, created_date, modified_date)\n      VALUES($1, $2, $3, $4, $5, $6, $7)\n      returning *";
              values = [(0, _uuid.v4)(), req.body.success, req.body.low_point, req.body.take_away, req.user.id, (0, _moment["default"])(new Date()), (0, _moment["default"])(new Date())];
              _context.prev = 2;
              _context.next = 5;
              return _index["default"].query(createQuery, values);

            case 5:
              _yield$db$query = _context.sent;
              rows = _yield$db$query.rows;
              return _context.abrupt("return", res.status(201).send(rows[0]));

            case 10:
              _context.prev = 10;
              _context.t0 = _context["catch"](2);
              return _context.abrupt("return", res.status(400).send(_context.t0));

            case 13:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[2, 10]]);
    }))();
  },

  /**
   * Get All Reflections
   * @param {object} req 
   * @param {object} res 
   * @returns {object} reflections array
   */
  getAll: function getAll(req, res) {
    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
      var findAllQuery, _yield$db$query2, rows, rowCount;

      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              findAllQuery = 'SELECT * FROM reflections where owner_id = $1';
              _context2.prev = 1;
              _context2.next = 4;
              return _index["default"].query(findAllQuery, [req.user.id]);

            case 4:
              _yield$db$query2 = _context2.sent;
              rows = _yield$db$query2.rows;
              rowCount = _yield$db$query2.rowCount;
              return _context2.abrupt("return", res.status(200).send({
                rows: rows,
                rowCount: rowCount
              }));

            case 10:
              _context2.prev = 10;
              _context2.t0 = _context2["catch"](1);
              return _context2.abrupt("return", res.status(400).send(_context2.t0));

            case 13:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[1, 10]]);
    }))();
  },

  /**
   * Get A Reflection
   * @param {object} req 
   * @param {object} res
   * @returns {object} reflection object
   */
  getOne: function getOne(req, res) {
    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
      var text, _yield$db$query3, rows;

      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              text = 'SELECT * FROM reflections WHERE id = $1 AND owner_id = $2';
              _context3.prev = 1;
              _context3.next = 4;
              return _index["default"].query(text, [req.params.id, req.user.id]);

            case 4:
              _yield$db$query3 = _context3.sent;
              rows = _yield$db$query3.rows;

              if (rows[0]) {
                _context3.next = 8;
                break;
              }

              return _context3.abrupt("return", res.status(404).send({
                'message': 'reflection not found'
              }));

            case 8:
              return _context3.abrupt("return", res.status(200).send(rows[0]));

            case 11:
              _context3.prev = 11;
              _context3.t0 = _context3["catch"](1);
              return _context3.abrupt("return", res.status(400).send(_context3.t0));

            case 14:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, null, [[1, 11]]);
    }))();
  },

  /**
   * Update A Reflection
   * @param {object} req 
   * @param {object} res 
   * @returns {object} updated reflection
   */
  update: function update(req, res) {
    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
      var findOneQuery, updateOneQuery, _yield$db$query4, rows, values, response;

      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              findOneQuery = 'SELECT * FROM reflections WHERE id=$1 AND owner_id = $2';
              updateOneQuery = "UPDATE reflections\n      SET success=$1,low_point=$2,take_away=$3,modified_date=$4\n      WHERE id=$5 AND owner_id = $6 returning *";
              _context4.prev = 2;
              _context4.next = 5;
              return _index["default"].query(findOneQuery, [req.params.id, req.user.id]);

            case 5:
              _yield$db$query4 = _context4.sent;
              rows = _yield$db$query4.rows;

              if (rows[0]) {
                _context4.next = 9;
                break;
              }

              return _context4.abrupt("return", res.status(404).send({
                'message': 'reflection not found'
              }));

            case 9:
              values = [req.body.success || rows[0].success, req.body.low_point || rows[0].low_point, req.body.take_away || rows[0].take_away, (0, _moment["default"])(new Date()), req.params.id, req.user.id];
              _context4.next = 12;
              return _index["default"].query(updateOneQuery, values);

            case 12:
              response = _context4.sent;
              return _context4.abrupt("return", res.status(200).send(response.rows[0]));

            case 16:
              _context4.prev = 16;
              _context4.t0 = _context4["catch"](2);
              return _context4.abrupt("return", res.status(400).send(_context4.t0));

            case 19:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, null, [[2, 16]]);
    }))();
  },

  /**
   * Delete A Reflection
   * @param {object} req 
   * @param {object} res 
   * @returns {void} return statuc code 204 
   */
  "delete": function _delete(req, res) {
    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
      var deleteQuery, _yield$db$query5, rows;

      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              deleteQuery = 'DELETE FROM reflections WHERE id=$1 AND owner_id = $2 returning *';
              _context5.prev = 1;
              _context5.next = 4;
              return _index["default"].query(deleteQuery, [req.params.id, req.user.id]);

            case 4:
              _yield$db$query5 = _context5.sent;
              rows = _yield$db$query5.rows;

              if (rows[0]) {
                _context5.next = 8;
                break;
              }

              return _context5.abrupt("return", res.status(404).send({
                'message': 'reflection not found'
              }));

            case 8:
              return _context5.abrupt("return", res.status(204).send({
                'message': 'deleted'
              }));

            case 11:
              _context5.prev = 11;
              _context5.t0 = _context5["catch"](1);
              return _context5.abrupt("return", res.status(400).send(_context5.t0));

            case 14:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, null, [[1, 11]]);
    }))();
  }
};
var _default = Reflection;
exports["default"] = _default;
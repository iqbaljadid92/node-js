"use strict";

var _express = _interopRequireDefault(require("express"));

var _dotenv = _interopRequireDefault(require("dotenv"));

require("babel-polyfill");

var _Reflection = _interopRequireDefault(require("../src/usingJSObject/controllers/Reflection"));

var _Reflection2 = _interopRequireDefault(require("../src/usingDB/controllers/Reflection"));

var _Auth = _interopRequireDefault(require("../src/usingDB/middleware/Auth"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// // server.js
// import express from 'express';
// import ReflectionJSObject from '../src/usingJSObject/controllers/Reflection';
// import ReflectionusingDB from '../src/usingDB/controllers/Reflection';
// import 'babel-polyfill';
// const app = express()
// app.use(express.json())
// app.get('/', (req, res) => {
//   return res.status(200).send({'message': 'YAY! Congratulations! Your first endpoint is working'});
// })
// app.post('/api/v1/reflections', ReflectionusingDB.create);
// app.get('/api/v1/reflections', ReflectionusingDB.getAll);
// app.get('/api/v1/reflections/:id', ReflectionusingDB.getOne);
// app.put('/api/v1/reflections/:id', ReflectionusingDB.update);
// app.delete('/api/v1/reflections/:id', ReflectionusingDB.delete);
// app.listen(3000)
// console.log('app running on port ', 3000);
// server.js
// import UserWithDb from '../src/usingDB/controllers/Users';
_dotenv["default"].config();

var Reflection = process.env.TYPE === 'db' ? _Reflection2["default"] : _Reflection["default"];
var app = (0, _express["default"])();
app.use(_express["default"].json());
app.get('/', function (req, res) {
  return res.status(200).send({
    'message': 'YAY! Congratulations! Your first endpoint is working'
  });
});
app.post('/api/v1/reflections', _Auth["default"].verifyToken, Reflection.create);
app.get('/api/v1/reflections', _Auth["default"].verifyToken, Reflection.getAll);
app.get('/api/v1/reflections/:id', _Auth["default"].verifyToken, Reflection.getOne);
app.put('/api/v1/reflections/:id', _Auth["default"].verifyToken, Reflection.update);
app["delete"]('/api/v1/reflections/:id', _Auth["default"].verifyToken, Reflection["delete"]); // app.post('/api/v1/users', UserWithDb.create);
// app.post('/api/v1/users/login',UserWithDb.login);
// app.delete('/api/v1/users/me', Auth.verifyToken, UserWithDb.delete);

app.listen(3000);
console.log('app running on port ', 3000);
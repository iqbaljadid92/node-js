"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _uuid = _interopRequireDefault(require("uuid"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Reflection = /*#__PURE__*/function () {
  /**
   * class constructor
   * @param {object} data
   */
  function Reflection() {
    _classCallCheck(this, Reflection);

    this.reflections = [];
  }
  /**
   * 
   * @returns {object} reflection object
   */


  _createClass(Reflection, [{
    key: "create",
    value: function create(data) {
      var newReflection = {
        id: _uuid["default"].v4(),
        success: data.success || '',
        lowPoint: data.lowPoint || '',
        takeAway: data.takeAway || '',
        createdDate: _moment["default"].now(),
        modifiedDate: _moment["default"].now()
      };
      this.reflections.push(newReflection);
      return newReflection;
    }
    /**
     * 
     * @param {uuid} id
     * @returns {object} reflection object
     */

  }, {
    key: "findOne",
    value: function findOne(id) {
      return this.reflections.find(function (reflect) {
        return reflect.id === id;
      });
    }
    /**
     * @returns {object} returns all reflections
     */

  }, {
    key: "findAll",
    value: function findAll() {
      return this.reflections;
    }
    /**
     * 
     * @param {uuid} id
     * @param {object} data 
     */

  }, {
    key: "update",
    value: function update(id, data) {
      var reflection = this.findOne(id);
      var index = this.reflections.indexOf(reflection);
      this.reflections[index].success = data['success'] || reflection.success;
      this.reflections[index].lowPoint = data['lowPoint'] || reflection.lowPoint;
      this.reflections[index].takeAway = data['takeAway'] || reflection.takeAway;
      this.reflections[index].modifiedDate = _moment["default"].now();
      return this.reflections[index];
    }
    /**
     * 
     * @param {uuid} id 
     */

  }, {
    key: "delete",
    value: function _delete(id) {
      var reflection = this.findOne(id);
      var index = this.reflections.indexOf(reflection);
      this.reflections.splice(index, 1);
      return {};
    }
  }]);

  return Reflection;
}();

var _default = new Reflection();

exports["default"] = _default;